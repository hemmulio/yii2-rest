<?php
return [
    //-- general buttons --//
    'There is no user with such email.' => 'Пользователя с таким email не найдено',
    'There is no user with such token.' => 'Пользователя с таким токеном не найдено',
];
