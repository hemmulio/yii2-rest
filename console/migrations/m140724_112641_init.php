<?php

use yii\db\Schema;
use yii\db\Migration;

class m140724_112641_init extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';
        }

        $this->createTable(
            '{{%user_backend}}',
            [
                'id' => Schema::TYPE_PK,
                'username' => Schema::TYPE_STRING . ' NOT NULL ',
                'email' => Schema::TYPE_STRING . ' NOT NULL',
                'auth_key' => Schema::TYPE_STRING . '(32) NOT NULL',
                'password_hash' => Schema::TYPE_STRING . ' NOT NULL',
                'password_reset_token' => Schema::TYPE_STRING,
                'created_at' => Schema::TYPE_INTEGER . ' NOT NULL',
                'updated_at' => Schema::TYPE_INTEGER . ' NOT NULL',
            ],
            $tableOptions
        );

        $this->createTable(
            '{{%user_rest}}',
            [
                'id' => Schema::TYPE_PK,
                'name' => Schema::TYPE_STRING . ' NOT NULL ',
                'secondname' => Schema::TYPE_STRING,
                'email' => Schema::TYPE_STRING . ' NOT NULL',
                'phone' => Schema::TYPE_STRING . ' NOT NULL',
                'password_hash' => Schema::TYPE_STRING . ' NOT NULL',
                'password_reset_token' => Schema::TYPE_STRING,
                'created_at' => Schema::TYPE_INTEGER . ' NOT NULL',
                'updated_at' => Schema::TYPE_INTEGER . ' NOT NULL',
            ],
            $tableOptions
        );

        $user_backend = new \backend\models\User();
        $user_backend->username = 'demo';
        $user_backend->email = 'demo@mail.net';
        $user_backend->setPassword('demo');
        $user_backend->generateAuthKey();

        $user_rest = new \rest\models\User();
        $user_rest->name = 'Вася';
        $user_rest->secondname = 'Пупкин';
        $user_rest->email = 'demo@mail.net';
        $user_rest->phone = '89654854877';
        $user_rest->setPassword('demo');


        return $user_backend->insert() && $user_rest->insert();
    }

    public function down()
    {
        $this->dropTable('{{%user_backend}}');
        $this->dropTable('{{%user_rest}}');
    }
}
