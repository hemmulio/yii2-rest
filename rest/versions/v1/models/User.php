<?php

namespace rest\versions\v1\models;

use rest\models\User as RestUser;
use yii\filters\RateLimitInterface;

/**
 * This is the model class for table "tbl_user". *
 */
class User extends RestUser implements RateLimitInterface
{
    public $password;

    public function getRateLimit($request, $action)
    {
        if (($request->isPut || $request->isDelete || $request->isPost)) {
            return [1, 1];
        }

        return [2, 1];
    }

    public function loadAllowance($request, $action)
    {
        return [
            \Yii::$app->cache->get($request->getPathInfo() . $request->getMethod() . '_remaining'),
            \Yii::$app->cache->get($request->getPathInfo() . $request->getMethod() . '_ts')
        ];
    }

    public function saveAllowance($request, $action, $allowance, $timestamp)
    {
        \Yii::$app->cache->set($request->getPathInfo() . $request->getMethod() . '_remaining', $allowance);
        \Yii::$app->cache->set($request->getPathInfo() . $request->getMethod() . '_ts', $timestamp);
    }

    public function rules()
    {
        return [
            [['name', 'email', 'phone', 'password' ], 'required', 'on' => 'create',],
            [
                ['email', 'phone',], 'unique',
                'targetClass' => '\rest\versions\v1\models\User',
            ],
            [['name', 'secondname', 'phone'], 'string', 'min' => 2, 'max' => 255],
            ['email', 'email'],

        ];
    }

    public function fields(){
        return [
            "id",
            "secondname",
            "name",
            "email",
            "phone",
        ];
    }

    public function beforeSave($insert) {
        if (parent::beforeSave($insert)) {
            if (isset($this->password)) {
                $this->setPassword($this->password);
            }


            return true;
        } else {
            return false;
        }
    }


}
