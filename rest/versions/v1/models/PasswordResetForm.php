<?php
namespace rest\versions\v1\models;
use yii\base\Model;
use Yii;
/**
 * Password reset request form.
 */
class PasswordResetForm extends Model
{
    public $token;
    public $password;
    /**
     * Returns the validation rules for attributes.
     *
     * @return array
     */
    public function rules()
    {
        return [
            [['token', 'password'], 'required'],
            ['token', 'exist',
                'targetClass' => '\rest\versions\v1\models\User',
                'targetAttribute' => 'password_reset_token',
                'message' => Yii::t('app', 'There is no user with such token.')
            ],
        ];
    }
    /**
     * Sends an token with a link, for resetting the password.
     *
     * @return bool Whether the token was send.
     */
    public function resetPasswd()
    {
        /* @var $user User */
        $user = User::findOne([
            'password_reset_token' => $this->token,
        ]);

        $user->setPassword($this->password);
        $user->password_reset_token = null;
        $user->save();
        return $user;

    }
}
