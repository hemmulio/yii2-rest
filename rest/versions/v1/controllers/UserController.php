<?php
namespace rest\versions\v1\controllers;

use rest\versions\v1\models\User;
use rest\versions\v1\models\PasswordResetRequestForm;
use rest\versions\v1\models\PasswordResetForm;
use yii\rest\Controller;
use yii\data\ActiveDataProvider;
use yii\filters\RateLimiter;
use yii\filters\auth\HttpBasicAuth;
use yii\web\ForbiddenHttpException;
use Yii;


/**
 * Class UserController
 * @package rest\versions\v1\controllers
 */
class UserController extends Controller
{
    public $modelClass = 'rest\versions\v1\models\User';
    // public $updateScenario = 'update';

    public function behaviors() {
        $behaviors = parent::behaviors();

        $behaviors['rateLimiter'] = [
            'class' => RateLimiter::className(),
            'enableRateLimitHeaders' => true,
        ];

        $behaviors['authenticator'] = [
            'class' => HttpBasicAuth::className(),
            'only' => ['update', 'delete'],
            'auth' => [$this, 'auth']
        ];

        return $behaviors;
    }
    public function actions() {
        //unset($actions['delete']);

        return array_merge(
            parent::actions(),
            [
                'index' => [
                    'class' => 'yii\rest\IndexAction',
                    'modelClass' => $this->modelClass,
                   // 'checkAccess' => [$this, 'checkAccess'],
                    'prepareDataProvider' => function ($action) {
                        /* @var $model Post */
                        $model = new $this->modelClass;
                        $query = $model::find();
                        $dataProvider = new ActiveDataProvider(['query' => $query]);
                        // $model->setAttribute('title', @$_GET['title']);
                        // $query->andFilterWhere(['like', 'title', $model->title]);
                        return $dataProvider;
                    }
                ]
            ],
            [
                'view' => [
                    'class' => 'yii\rest\ViewAction',
                    'modelClass' => $this->modelClass,
                ]
            ],
            [
                'create' => [
                    'class' => 'yii\rest\CreateAction',
                    'scenario' => 'create',
                    'modelClass' => $this->modelClass,
                ]
            ],
            [
                'update' => [
                    'class' => 'yii\rest\UpdateAction',
                    'modelClass' => $this->modelClass,
                    'checkAccess' => [$this, 'checkAccess'],
                ]
            ],
            [
                'delete' => [
                    'class' => 'yii\rest\DeleteAction',
                    'modelClass' => $this->modelClass,
                    'checkAccess' => [$this, 'checkAccess'],
                ]
            ]
        );
    }

    public function actionPasswordResetRequest() {
        $model = new PasswordResetRequestForm();
        $model->load(Yii::$app->getRequest()->getBodyParams(), '');

        if ($model->validate()) {
            //jодин хер не отлавливается...
            try {
                $model->sendEmail();
            } catch (Exception $e) {
                throw new ServerErrorHttpException('Sorry, we are unable to reset password for email provided.');
            }
            Yii::$app->getResponse()->setStatusCode(202);
        } else {
            return $model;
        }
    }

    public function actionPasswordReset() {
        $model = new PasswordResetForm();
        $model->load(Yii::$app->getRequest()->getBodyParams(), '');

        if ($model->validate()) {
            return $model->resetPasswd();
        } else {
            return $model;
        }

    }



    public function checkAccess($id, $model) {

        if ($model->id !== Yii::$app->user->identity->id) {
            throw new ForbiddenHttpException('Forbidden');
        }
        return true;
    }


    public function Auth($username, $password) {
        if(empty($username) || empty($password)){
            return null;
        }

        $user = User::findOne([
            'email' => $username,
        ]);

        if(empty($user)){
            return null;
        }

        $isPass = $user->validatePassword($password);

        if(!$isPass){
            return null;
        }

        return $user;
    }

}
